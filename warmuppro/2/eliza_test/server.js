var express = require('express');
var assert=require('assert');		
var functions=require('./script/functions.js');	// my functions goes here
var nodemailer = require('nodemailer');	//send email
var bodyparser=require('body-parser');	//parser the body of dom
var mongoose=require('mongoose');	// handler database
var session=require('client-sessions'); //handle sessions
var bcrypt=require('bcryptjs');    	// use for encrypt the password
// var request = require('request');   // this is use for recaptcha
var keys=require('random-key');



//-------------------modules ends---------------------------------------

//-------------------config ends---------------------------------------


var app =express();

var status={status:'',message:''};



var Schema=mongoose.Schema;
var ObjectId=Schema.ObjectId;
var User=mongoose.model('User',new Schema({
	id : ObjectId,
	username: {type:String, unique: true,require:true},
	email: { type: String, unique: true},
	password:String,
	key:String,  // not yet provided
	disable: Boolean,
	conversations:[
	{
		id:Number,
		start_date:Date,
		conversation:[
		{
			timestamp:Date,
			name:String,
			text:String
		}
		]
	}
	]
}));


var backdoor_key='abracadabra';
var port=8080;
var url= 'mongodb://localhost:27017/wp2';
var eliza={eliza:''};
var trans_data=[];
var eliza_res=['don\'t stop,keep saying...',
				'sure, you are right',
				'that\'s right',
				'what\'s happended',
				'what\'s else ?',
				'what do you mean by that?',
				'continue....'
];


app.use(express.static(__dirname));
app.use(bodyparser.urlencoded({extended: true}));
app.use(bodyparser.json());
app.use(express.static(__dirname));
app.use(session({
	cookieName:'session',
	secret: 'lksdjf98sdf90sdjf0sdfLKJDF)(J09fsdfj90sdf09sdfj',
	duration: 30*60*1000,
	activeDuration: 5*60*1000,
	httpOnly: true,
	//secure,ephemeral
}));



// connect to database
mongoose.connect(url);


//middleware for check session ..

app.use(function(req,res,next){
	if(req.session && req.session.user){
		User.findOne({name:req.session.user.name},function(err,user){
			if(user){
				req.user=user;
				delete req.user.password;
				req.session.user=req.user;
				//req.locals.user=req.user;
			}
			next();
		});
	}else{
		next();
	}
});



app.set('views','./views');
app.set('view engine','pug');
app.listen(port,function(){
	console.log('server listen on port ' + port);
});


//-------------------config ends---------------------------------------

//--------------------functions begin----------------------------------------



function requireLogin(req,res,next){
	if(!req.user){
		res.redirect('/login');
	}	
	else{
		next();
	}
}



//-------------------------functions end-----------------------------------


//-------------------------routes begin-----------------------------------



//good
app.get('/login',function(req,res){
	res.render('login');
});
//good
app.get('/eliza',requireLogin,function(req,res){
//list out all the conversation of one single session.
	res.render('eliza',{name:req.session.user.username,date:new Date()});
});

app.get('/verify',function(req,res){
	var email=req.query.email;
	var key=req.query.key;

console.log(email+"----"+key);

	if(email==null || email==undefined || email=='' || key==null || key==undefined || key=='' ){
		res.render('verify');	
	}
	else{
		
		User.findOne({email:email},function(err,user){
		if(!user){
			status.status='ERROR';
			status.message='user doesn\'t exist ';
			res.send(status);
		}else{
			if(key ===user.key || key==backdoor_key){
				user.disable=false;
				user.save();
				req.session.user=user;
				status.status='OK';
				status.message='';
				res.send(status);
				}else{
				status.status='ERROR';
				status.message='key verify error';
				res.send(status);
			}
		}
	});

	}
	
});



app.get('/logout',requireLogin,function(req,res){
		req.session.reset();
		res.send(JSON.stringify({status:'OK'}));
	
});

app.post('/logout',requireLogin,function(req,res){
		req.session.reset();
		res.send(JSON.stringify({status:'OK'}));
	
});



app.post('/login',function(req,res){
	status.status='';
	status.message='';
	req.session.reset();
	var name=req.body.username;
	var pw=req.body.password;


	User.findOne({username:name},function(err,user){
		
		if(!user){
			status.status='ERROR';
			status.message='name don not exist';
			res.send(status);
		}else{
			// if(!bcrypt.compareSync(pw,user.password)){
				if(pw!=user.password){
				status.status='ERROR';
				status.message='pw incorrect';
				res.send(status);
			}else{
				if (user.disable==true){
					status.status='ERROR';
					status.message='email unverify';
					res.send(status);
				}else{
					var id=keys.generateDigits(20);
					var start_date=new Date();
					var conversation=[];
					var tt={id,start_date,conversation};
					// tt.id=id;
					// tt.start_date=start_date;
					// tt.conversation=[]
					//test
					//	console.log(tt);
					//test
					user.conversations.push(tt);
					user.save();
					req.session.user=user;
					
					//console.log("post login ok findone user-------->"+req.session.user);
					status.status='OK';
					res.send(status);
				}
			}
		} 
	});
});








app.post('/listconv',function(req,res){

var rt={};
rt.status='';
rt.conversations=[];

if(req.session && req.session.user){
for(var i=0;i<req.session.user.conversations.length;i++){
	
	var id=req.session.user.conversations[i].id;
	var start_date=req.session.user.conversations[i].start_date;
//	console.log({id,start_date});
	rt.conversations.push({id,start_date});	
}
rt.status='OK';
res.send(rt);
}else{

	rt.status='ERROR';
	res.send(rt);

}






});





app.post('/getconv',function(req,res){
var conversation=[];
var status='';

if(req.session && req.session.user){

	conversation=req.session.user.conversations[req.session.user.conversations.length-1].conversation;
	status='OK';
	var rt={status,conversation};
	res.send(rt);

}else{

 	status='ERROR';
	var rt={status,conversation};
	res.send(rt);

}




	
});



app.post('/DOCTOR',function(req,res){
	
	
	User.findOne({username:req.session.user.username},function(err,user){
		


	if(user.conversations.length>0){
		var tm1=new Date();
		var chatinput = req.body.human;
		var name1=user.username;
		

		var mod1={timestamp:'',name:'',text:''};
		var mod2={timestamp:'',name:'',text:''};
		mod1.timestamp=tm1;
		mod1.name=name1;
		mod1.text=chatinput;
		user.conversations[user.conversations.length-1].conversation.push(mod1);
		

		var elizainput=eliza_res[Math.floor((Math.random()*eliza_res.length))];
		mod2.timestamp=new Date();
		mod2.name='eliza';
		mod2.text=elizainput;

		user.conversations[user.conversations.length-1].conversation.push(mod2);
		user.save();

		var retu={status:'OK',eliza:''};
		retu.eliza=elizainput
		res.send(retu);

	}else{
		res.send({status:'ERROR'});
	}

	});
});
	






app.post('/verify',function(req,res){
	status.status='';
	status.message='';
	var key=req.body.key;
	var email=req.body.email;

	User.findOne({email:email},function(err,user){
		if(!user){
			status.status='ERROR';
			status.message='user doesn\'t exist ';
			res.send(status);
		}else{
			if(req.body.key ===user.key || req.body.key==backdoor_key){
				user.disable=false;
				user.save();
				req.session.user=user;
				status.status='OK';
				status.message='';
				res.send(status);
				}else{
				status.status='ERROR';
				status.message='key verify error';
				res.send(status);
			}
		}
	});
});


app.get('/adduser',function(req,res){
	res.render('adduser');
});

app.post('/adduser',function(req,res){
	
	//var hashpw=bcrypt.hashSync(req.body.pw,bcrypt.genSaltSync(10));
	var hashpw=req.body.password;
	var re=functions.checkValidNameEmailPassword(req.body.username,req.body.password,req.body.email);

	if(re){
		User.findOne({name:req.body.username},function(err,user){
			if(user){
				status.status='ERROR';
				status.message='name exist'+req.body.username;
				res.send(status);					
			}
			else{
				User.findOne({email:req.body.email},function(err,user2){
					if(user2){
						status.status='ERROR';
						status.message='email exist';
						res.send(status);	
					}else{
							//------------------------recaptcha

 					//  			console.log(req.body);
 					// 		if(req.body['g-recaptcha-response'] === undefined || req.body['g-recaptcha-response'] === '' || req.body['g-recaptcha-response'] === null) {
					// 			status.status='ERROR';
					// 			status.message='recaptcha error1';
					// 			res.send(status);	
					//  		 }else{
					// 	 // Put your secret key here.
					// 	  var secretKey = "6LcIKBcUAAAAAPzlgoO3KzPtJlSeGV8Bv_XNiBz5";
					// 	  // req.connection.remoteAddress will provide IP address of connected user.
					// 	  var verificationUrl ="https://www.google.com/recaptcha/api/siteverify?secret=" + secretKey + "&response=" + req.body['g-recaptcha-response'] +"&remoteip=" + req.connection.remoteAddress;
					// 	  // Hitting GET request to the URL, Google will respond with success or error scenario.
					// 	  request(verificationUrl,function(error,response,body) {
					// 	    body = JSON.parse(body);
					//     // Success will be true or false depending upon captcha validation.
					//  	   if(body.success !== undefined && !body.success) {
					// 				status.status='ERROR';
					// 			status.message='recaptcha error2';
					// 			res.send(status);	
					// 	 }else{

									var key=keys.generate();
									var user=new User({
										username: req.body.username,
										email:req.body.email,
										password:hashpw,
										disable:true,
										key:key,
									});
									user.conversations=[];

									user.save(function(err){
										if(err){
											status.status='ERROR';
											status.message='data saving error';
											res.send(status);
										}							
									}); 
									//console.log('adduser post user after save'+user);
									var transporter = nodemailer.createTransport({
										service: 'Gmail',
										auth: {
           				 				user: 'shuangchen@cs.stonybrook.edu', 
            							pass: 'KGvc11:routine'
        								}
									});
									var text = 'please do not reply to this email \n\n' + 'here is the key for your verification :   '+ key +'\n\n'+
									'or you can click on the link here:'+ 'http://shuangchen.cse356.compas.cs.stonybrook.edu/verify?email='+req.body.email+'&key='+key;
									//kgvc: need to send links inside email..

									var mailOptions = {
    									from: 'no-reply@stonybrook.edu', 
    									to: req.body.email, 
    									subject: 'verification key', 
    									text: text 
									};

									transporter.sendMail(mailOptions, function(error, info){
    									if(error){
      					  					status.status='ERROR';
      					  					status.message='email sending error';
											res.send(status);
   							 			} 
									});

									status.status='OK';
									res.send(status);
								}
							});
			//			}
					}
				
			});
		
	}else{
		status.status='ERROR';
		status.message='input fields error';
		res.send(status);
	}
});






//-------------------------routes end-----------------------------------
















